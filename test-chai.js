const chai = require('chai');
const expect = chai.expect;
const math = require('./math');
describe('Test chai', ()=>{
    it('should compare thing by expect', ()=>{
        expect(1).to.equal(1);
    });
    it('should compare another thing by expect', ()=>{
        expect(5>8).to.be.false;
        expect({name: 'winwalkerz'}).to.deep.equal({name: 'winwalkerz'});
        expect({name: 'winwalkerz'}).to.have.property('name').to.equal('winwalkerz');
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('winwalkerz').to.be.a('string');
        expect('winwalekrz'.length).to.equal(10);
        expect('winwalkerz').to.lengthOf(10);
        expect([1,2,3]).to.lengthOf(3);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;
    });
});
describe('Math module',()=>{
    context('Function add1', ()=>{
        it('ควรส่งค่ากลับเ็นตัวเลข', ()=>{
            expect(math.add1(0,0)).to.be.a('number')
        });
        it('add(1,1) ควรส่งค่ากลับมาเป็น 2', ()=>{
            expect(math.add1(1,1)).to.equal(2);
        });
    });
});